import React from 'react';
import './App.css';
import './base.css';
import Header from './components/Header';
import SearchOptions from './components/SearchOptions';
import HotelDetails from './components/HotelDetails';

import { ThemeProvider } from 'styled-components';
import theme from './theme'
function App() {
    return (
        <ThemeProvider theme={theme}>
            <div className="App">
                <Header />
                <SearchOptions />
                <HotelDetails />
            </div>
        </ThemeProvider>
        
    );
}

export default App;
