import React, { useState, useEffect } from 'react'
import data from './data/carousel.json'
import { StyledCarousel, CarouselContainer, LeftArrow, RightArrow, ANIMATION_DURATION} from './styles';

const Carousel = () => {
    //animating state for css transition and callback
    const [animating, setAnimating] = useState(false)

    //state for the current index and container translateX
    const [slide, setSlide] = useState({
        index: 1,
        scale: "-100%"
    });

    //state for the real carousel slides
    const [images, setImages] = useState([
        data.imageUrls[data.imageUrls.length - 1], 
        ...data.imageUrls,
        data.imageUrls[0]
    ], []);

    useEffect(() => {
        setTimeout(() => {
            //callback when reach at start
            if (slide.index === 0) {
                setSlide({
                    index: images.length - 2,
                    scale: `-${(images.length - 2)*100}%`
                })
            }

            //callback when reach the end
            if (slide.index === images.length - 1) {
                setSlide({
                    index: 1,
                    scale: "-100%"
                })
            }

            //remove transition class
            setAnimating(false)
        }, ANIMATION_DURATION*1000);
    }, [slide.index]);

    const goLeft = () => {
        if (animating) return;
        setAnimating(true)
        setSlide({
            index: slide.index + 1,
            scale: `-${(slide.index + 1)*100}%`
        });
    }

    const goRight = () => {
        if (animating) return;
        setAnimating(true)
        setSlide({
            index: slide.index - 1,
            scale: `-${(slide.index - 1)*100}%`
        });
    }

    return (
        <StyledCarousel>
            <CarouselContainer scaleX={slide.scale} className={animating ? 'animating' : ''}>
                {images.map((image, index) => (
                    <img 
                        src={image} 
                        key={index} 
                        alt={`carousel-image-${index}`} 
                        itemProp="image" 
                        srcSet={`${image} 2000px`}
                        sizes="100vw"
                    />
                ))}
            </CarouselContainer>
            <LeftArrow onClick={goLeft}>{"<-"}</LeftArrow>
            <RightArrow onClick={goRight}>{"->"}</RightArrow>
        </StyledCarousel>
    )
}

export default Carousel;

