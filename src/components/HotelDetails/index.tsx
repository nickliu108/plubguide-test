import React, { FC } from 'react'
import Carousel from './Carousel'
import styled from 'styled-components';
import Facility from './facility'
import Location from './location'

const StyledHotelName = styled.div`
    font-size: 34px;
    font-weight: bold;
    padding: 20px 0;
    text-align:center;
    font-family: serif;
`;

const StyledHotelDetails = styled.div`
    background-color: #f6ded2;
    padding: 40px;
`;

const HotelDetails:FC = () => {
    return (
        <StyledHotelDetails>
            <StyledHotelName itemProp="name">Monsieur Didot</StyledHotelName>
            <Facility />
            <Location />
            <Carousel />
        </StyledHotelDetails>
    )
}



export default HotelDetails;
