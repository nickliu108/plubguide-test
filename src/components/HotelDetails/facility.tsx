import React, { FC } from 'react'
import {StyledList} from './styles'

const data = ['4 people', '2 bedroom', '2 bathrooms', 'Private Terrasse', 'Peaceful']

const Facility = () => {
    return (
        <StyledList>
            {data.map((facility, index) => (
                <div key={index}>{facility}</div>
            ))}
        </StyledList>
    )
}

export default Facility;
