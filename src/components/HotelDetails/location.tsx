import React, { FC } from 'react'
import { StyledLocation } from './styles';

const data = ['NOtting Hill, London', 'Wlk 6 mins (Westbourne Park Station)', 'Stairs']

const Location = () => {
    return (
        <StyledLocation>
            {data.map((location, index) => (
                <div key={index}>{location}</div>
            ))}
        </StyledLocation>
    )
}

export default Location;
