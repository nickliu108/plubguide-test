import styled from 'styled-components';

export const ANIMATION_DURATION = '0.5';

export const StyledList = styled.div`
    display: flex;
    justify-content: center;
    padding: 10px;
    border-bottom: 1px solid ${props => props.theme.lineColor};

    @media ${props => props.theme.mediaQuery.tablet} { 
        width: 60%;
        margin: 0 auto;
    }


    > div {
        padding: 0 20px;
    }
`;

export const StyledCarousel = styled.div`
    width: 100%;
    position: relative;
    overflow: hidden;
    padding: 30px 0;
`;

export const CarouselContainer = styled.div`
    padding: 20px 0;
    display: flex;
    flex-wrap: nowrap;
    transform: ${props => props.scaleX ? `translateX(${props.scaleX})` : ""};

    > img {
        flex: 1 0 100%;
        width: calc(100vw - 80px);
        object-fit: contain;
    }

    &.animating {
        transition: ${ANIMATION_DURATION}s ease-out;
    }
`;

export const StyledControl = styled.div`
    display: block;
    width: 40px;
    height: 40px;
    position: absolute;
    font-size: 30px;
    top: 50%;
    transform: translateY(-50%);
    background-color: rgba(117, 190, 218, 0.5);
    :hover {
        cursor: pointer;
    }
`;

export const LeftArrow = styled(StyledControl)`
    
`;

export const RightArrow = styled(StyledControl)`
    right: 0;
`;

export const StyledLocation = styled(StyledList)`
    border-right: 1px soild ${props => props.theme.lineColor};
`;