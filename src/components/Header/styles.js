import styled from 'styled-components';

export const StyledHeader = styled.div`
    display:flex;
    justify-content: space-between;
    border-bottom: 1px solid ${props => props.theme.lineColor};
`;

export const StyledHamburger = styled.div`
    padding: 20px;
    line-height: 20px;
    border-right: 1px solid ${props => props.theme.lineColor};
`;


export const StyledUl = styled.ul`
    display:flex;
    line-height:20px;
`;

export const StyledLi = styled.li`
    padding:0 10px;
`;

export const StyledMenu = styled.div`
`

export const StyledLogo = styled.div`
    line-height: 60px;
    font-size: 20px;
    flex-grow: 2;
    text-align: center;
    white-space: nowrap;
`;

export const StyledSearch = styled.div`
    padding: 20px;
    border-left: 1px solid ${props => props.theme.lineColor};
`;

export const StyledLink = styled.a`
    padding: 0;
    color: #666;
    font-size:14px;  
    white-space: nowrap;
    line-height: 60px;

    @media ${props => props.theme.mediaQuery.tablet} { 
        padding: 20px;
    }

    &:hover {
        color: #111;
    }
`;