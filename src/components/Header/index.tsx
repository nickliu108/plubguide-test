import React, { FC } from 'react';
import { StyledHeader, StyledHamburger, StyledUl, StyledLi, StyledMenu, StyledLogo, StyledSearch, StyledLink} from './styles';

const Hamburger:FC = () => {
    return (
        <StyledHamburger className="Hamburger">=</StyledHamburger>
    );
}

const Menu:FC = props => {
    return (
        <StyledMenu>
            <StyledUl>
                <StyledLi>
                    <StyledLink itemProp="url">Homes</StyledLink>
                </StyledLi>
                <StyledLi>
                    <StyledLink itemProp="url">Hosts</StyledLink>
                </StyledLi>
            </StyledUl>
        </StyledMenu>
    )
}

const Logo:FC = () => {
    return (
        <StyledLogo>PLUMB GUIDE</StyledLogo>
    )
}
const Links:FC = () => {
    return (
        <div className = "Links">
            <StyledUl>
                <StyledLi>
                    <StyledLink itemProp="url">Need help?</StyledLink>
                </StyledLi>
                <StyledLi>
                    <StyledLink itemProp="url">Login</StyledLink>
                </StyledLi>
            </StyledUl>
        </div>
    )
}
const Search:FC = () => {
    return (
        <StyledSearch>Search</StyledSearch>
    )
}


const Header = () => {
    return (
        <StyledHeader>
            <Hamburger />
            <Menu />
            <Logo />
            <Links />
            <Search />
        </StyledHeader>
    );
}

export default Header;
