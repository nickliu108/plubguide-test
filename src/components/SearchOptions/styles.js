import styled from 'styled-components';

export const StyledSearchOptions = styled.div`
    padding: 20px;
    font-size: 12px;
    display: flex;
    justify-content: center;

    @media ${props => props.theme.mediaQuery.desktop} { 
        padding: 20px 40px;
    }


    > div {
        padding: 10px 0;
        border-right: 1px solid ${props => props.theme.lineColor};
        margin-right: 20px;
        flex-grow:4;
    }
`

export const StyledCTA = styled.button`
    padding: 20px;
    border: none;
    background: ${props => props.theme.primaryColor};
    
    :hover {
        cursor: pointer;
    }
`;