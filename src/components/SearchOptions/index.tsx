import React, { FC } from 'react';
import { StyledSearchOptions, StyledCTA} from './styles'

const SearchDate:FC = () => {
    return (
        <div>
            <div>
                    <div>From/To</div>
                    <div>3 Jan 2020 - 28 Feb 2020</div>
            </div>
        </div>
    );
}

const GuestNumber:FC = () => {
    return (
        <div>
            <div>
                    <div>For</div>
                    <div>2 Guests</div>
            </div>
        </div>
    )
}

const Price:FC = () => {
    return (
        <div>
            <div>£ Per night</div>
            <div>345</div>
        </div>
    )
}
const TotalPrice:FC = () => {
    return (
        <div>
            <div>£ Total for 54 nights</div>
            <div>18,630</div>
        </div>
    )
}
const BookingCTA:FC = () => {
    return (
        <StyledCTA>instant booking</StyledCTA>
    )
}

const SearchOptions = () => {
    return (
        <StyledSearchOptions>
            <SearchDate />
            <GuestNumber />
            <Price />
            <TotalPrice />
            <BookingCTA />
        </StyledSearchOptions>
    );
}

export default SearchOptions;
