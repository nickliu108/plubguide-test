const theme = {
    primaryColor: '#FFBC30',
    lineColor: '#ccc',
    secondaryColor: '',
    mediaQuery: {
        tablet: `(min-width: 768px)`,
        desktop: `(min-width: 1280px)`
    }
}

export default theme;